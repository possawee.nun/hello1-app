FROM node:alpine3.13

RUN mkdir /src
WORKDIR /src

COPY package.json /src/package.json

RUN npm run build

COPY hello-1.js /src/hello-1.js

EXPOSE 8000
CMD ["npm", "run", "start"]

